#/bin/sh
echo "[...] Checking internet connection [...]"
ping -q -c5 8.8.8.8 > /dev/null  #Test du ping et envoi du résultat dans /dev/null
if [ $? -eq 0 ] #Si le code de retour de la commande ping est égale à 0, ça veut dire que le ping a fonctionné
then     #alors l'ordinateur est connecté
   echo "[...] Internet access OK [...] "
   exit 0
 else    #sinon l'ordinateur n'esr pas connecté 
   echo "[/!\] Not connected to Internet [/!\]" 
   echo "[/!\] Please check configuration [/!\]"
   exit 1
fi

#ping -q : Mode silencieux
#ping -c5 : 5 est le nombre de pings à effectuer
#£? contient le code de retour du ping
#/dev/null est fichier vide ; le contenu n'est pas enregistré
