#!/bin/bash

f='first'
l='last'

if [ $1 = $f ] ; then      #Si l'argument est 'first'
	tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)
	#Affichage du fichier donné en argument
	#awk permet d'afficher uniquement la deuxième colonne
	#grep permet de garder uniquement les lignes contenant le texte en argument
	#cut -d  permet de couper la chaîne là où il y a des ':' 
	#cut -f1 permet de garder ce qui est avant les ':'
fi 


if [ $1 = $l ] ; then    #Si l'argument est 'last'   
	tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)
	#sort -r permet d'inverser la sortie
fi

date -d @$tstamp
#On affiche le timestamp sous forme de date 

