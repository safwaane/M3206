#!/bin/bash
mkdir -p /tmp/backup
tar -cvzf /tmp/backup/`date +%Y_%m_%d_%H%M`.tar.gz $1 > /dev/null 
echo "Création de l'archive : /tmp/backup/`date +%Y_%m_%d_%H%M`.tar.gz"
exit 0
