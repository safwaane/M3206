#!/bin/bash
HISTFILE=~/.bash_history    #Emplacement du fichier history
set -o history
history | awk '{print $2}' | uniq -c | sort -nr | head -$1
#L'utilisation de awk permet de ne garder que la deuxieme colonne
#uniq -c permet de rassembler les commandes qui se ressemblent en une seule
#sort -nr permet de trier par ordre décroissant
#$1 représente le nombre de commandes à afficher 
