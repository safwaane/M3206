#!/bin/bash
tstamp=$(cat $2 | awk '{print $2}' | grep $1 | cut -d ':' -f1 | head -1)
#Affichage du fichier donné en argument
#awk permet d'afficher uniquement la deuxième colonne
#grep permet de garder uniquement les lignes contenant le texte en argument
#cut -d  permet de couper la chaîne là où il y a des ':' 
#cut -f1 permet de garder ce qui est avant les ':'

date -d @$tstamp
#date -d @tstamp permet d'afficher un timestamp sous forme de date facile à lire 
