#!/bin/bash
FILE="tmp/out/$$"  #On travaille dans un dossier temporaire
GREP="/bin/grep"   
if [ "$(id -u)" != "0" ]; then   #Le ID de root est forcément 0, donc si le ID du user actuel est différent de 0, il n'est pas root
   echo "[/!\] Vous devez être super-utilisateur [/!\]"
   exit 0
else
   echo "[...] update database [...]"
   apt-get update
   echo "[...] upgrade system [...]"
   apt-get upgrade
fi


