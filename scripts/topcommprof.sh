#!/bin/bash
cat /home/rt/M3206/my_history | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c | sort -nr | head -$1
#L'utilisation de awk permet de ne garder que la deuxieme colonne
#cut -d  permet de couper en fonction des points virgules et -f2 permet de garder ce qui se trouve après la chaine de caractères
#uniq -c  permet de rassembler les commandes qui se ressemblent en une seule
#sort -nr  permet de trier par ordre décroissant
#$1 représente le nombre de commandes à afficher
