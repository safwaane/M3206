#!/bin/bash
tab=(git tmux vim htop)	                #Liste des paquets

for pkg in ${tab[@]}; do  		#Parcours de la liste
	dpkg -s $pkg > /dev/null        #On n'a pas besoin de la réponsse de la commande
	if [ $? -eq 0 ]; then  		#Si le code de retour = 0 => Le paquet est installé
		echo "[...] $pkg: installé [...]"    
	else 				
		echo "[/!\] $pkg: pas installé [/!\] lancer la commande apt-get install $pkg"
	fi  
done
exit 0
   

